# Quines in Go &#129322;

Ein *Quine* ist ein Programm, das seinen eigenen Quelltext ausgibt.

## Aufgabe: Schreiben Sie einen Quine in Go.

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/reinerh/dhbw/tel21a/goquines)