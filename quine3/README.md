# Quine mit mehrzeiligem String, der innerhalb von `main()` definiert wird

Dieser Quine ist ähnlich wie Quine 2, allerdings wird anstelle einer globalen Konstante eine Variable
verwendet, in der der Quelltext des Programms steht.

Die `fmt.Print()`-Anweisung, die das Programm ausgibt, Zerlegt den Code-String in zwei Slices und
fügt ihn dazwischen mit zwei Backticks noch ein weiteres Mal ein, um das Ergebnis zu erreichen.

Dies ist eine modifizierte Version von https://github.com/alangpierce/golang-quine.
