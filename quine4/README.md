# Mogel-Quine

Die Idee hier ist, den Quelltext der Datei einzulesen und auszugeben.
Dies funktioniert natürlich nur, wenn die Datei auch noch neben der compilierten Datei liegt.
D.h. er funktioniert hier im Repo, solange man `go run quine4.go` in diesem Ordner ausführt.
Compliert man die Datei aber mit `go build` und veröffentlicht das Binary ohne den Quelltext,
so wird es nicht mehr funktionieren.

Dies ist im engeren Sinn also eigentlich kein Quine.
Go bietet allerdings die Möglichkeit, Dateien mit dem gebauten Binary zusammen zu bündeln.
Auf diese Weise könnte man wieder den gewünschten Effekt erreichen. Siehe dazu Quine 5.
