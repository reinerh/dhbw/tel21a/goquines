# Quine mit mehrzeiligem String als globale Konstante

Die Idee bei diesem Quine ist, dass man sich eine globale Konstante definiert, in der das Programm steht.

Es wird zuerst die `main()`-Funktion geschrieben und die Konstante (`program`) anschließend definiert.
Der Trick dabei ist, dass man mittels zweier Backticks (`) einen mehrzeiligen String definiert, in dem tatsächlich
exakt das Programm inkl. aller Zeilenumbrüche steht. Nur die Zuweisung der Konstanten am Ende wiederholt man nicht.

Würde man diesen String in der `main()` zweimal hintereinander ausgeben, so würde fast exakt das Programm auf der Konsole
erscheinen. Nur die Backticks aus der Konstantendefinition würden noch fehlen.
Deshalb gibt es eine zweite Konstante für den Backtick und diese wird bei der Ausgabe mit eingebaut.

Dies ist eine modifizierte Version von https://github.com/adamryman/go-quine.
