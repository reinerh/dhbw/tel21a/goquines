package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func main() {
	// Liste der Quines, die zu prüfen sind.
	quineNumbers := []int{1, 2, 3, 4, 5}

	// Primitive Abfrage der Kommandozeilenargumente.
	// Annahme ohne weitere Überprüfung: Alle Argumente sind gültige Quine-Nummern.
	if len(os.Args) >= 2 {
		quineNumbers = []int{}
		for _, arg := range os.Args[1:] {
			num, _ := strconv.Atoi(arg)
			quineNumbers = append(quineNumbers, num)
		}
	}

	// Für jeden Quine Datei und Ausgabe lesen
	// (mit den Hilfsfunktionen unten) und vergleichen.
	for _, v := range quineNumbers {
		output := runQuine(v)
		code := fileContent(v)

		if output == code {
			fmt.Printf("Quine %d: [ok]\n", v)
		} else {
			fmt.Printf("\nCode:\n%s\n\nOutput:\n%s\n\n", code, output)
		}
	}
}

func runQuine(quineNumber int) string {
	// Verzeichnisse und Dateinamen bestimmen.
	workingdir, _ := os.Getwd()
	directory := fmt.Sprintf("%s/quine%d", workingdir, quineNumber)
	file := fmt.Sprintf("quine%d.go", quineNumber)

	// Ausführung von "go run" vorbereiten.
	goRunCmd := exec.Command("go", "run", file)
	goRunCmd.Dir = directory

	// "go run" mit der Quelldatei ausführen und das Ergebnis zurückgeben.
	output, err := goRunCmd.Output()
	if err != nil {
		fmt.Println("Fehler:", err)
	}
	return strings.TrimSpace(string(output))
}

func fileContent(quineNumber int) string {
	// Verzeichnisse und Dateinamen bestimmen.
	workingdir, _ := os.Getwd()
	directory := fmt.Sprintf("%s/quine%d", workingdir, quineNumber)
	file := fmt.Sprintf("quine%d.go", quineNumber)

	// Quelldatei einlesen und zurückgeben.
	program, _ := ioutil.ReadFile(fmt.Sprintf("%s/%s", directory, file))
	return strings.TrimSpace(string(program))
}
