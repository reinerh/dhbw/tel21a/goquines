# Quine mit `fmt.Printf()`

Der Quine in diesem Ordner verwendet `fmt.Printf()` mit einem Formatstring, der in sich selbst eingesetzt wird.
Der Formatstring ist eine String-Repräsentation des Programms, wobei an der Stelle, an der der String wiederholt werden
müsste, stattdessen `%q` verwendet wird. Dies führt dazu, dass der eingesetzt String an dieser Stelle in Anführungszeichen
steht. Setzt man diesen String in sich selbst ein, ergibt sich exakt das Programm.

Dies ist eine modifizierte Version von https://github.com/bgphipps/go-quine.
