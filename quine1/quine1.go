package main

import "fmt"

func main() {
	prog := "package main\n\nimport \"fmt\"\n\nfunc main() {\n\tprog := %q\n\tfmt.Printf(prog, prog)\n}\n"
	fmt.Printf(prog, prog)
}
