# Quine mit eingebetter Quelldatei

Dies ist wie Quine 4, nur dass hier die Einbettungsfunktionalität von Go genutzt wird,
mit der die Quelldatei in das Binary fest eincompiliert wird.
Dadurch kann die Binärdatei auch ohne den Quelltext verteilt werden und wird funktionieren.

Hier wird das Package `"embed"` geladen.
Es wird nicht im Quelltext verwendet, aber in einer sogenannten *Direktive* im Kommentar
über der Variablendefinition. Dies führt dazu, dass der Quelltext in der Variablen `program`
verfügbar ist.
