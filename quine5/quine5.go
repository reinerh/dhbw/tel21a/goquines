package main

import (
	_ "embed"
	"fmt"
)

//go:embed quine5.go
var program string

func main() {
	fmt.Print(program)
}
